# forEach API Rest

## Instalación de todos los componentes necesarios para un correcto funcionamiento de la API en entorno local.
```
1. Instalar lenguaje de programación PHP (versión 7.3.21 en adelante).

En el siguiente link se muestran los pasos a seguir para su correcta instalación en Windows:
https://www.scriptcase.net/docs/es_es/v9/manual/02-scriptcase-installation/05-windows_php/

Para Linux, el link sería el siguiente:
https://www.scriptcase.net/docs/es_es/v9/manual/02-scriptcase-installation/06-linux_php/

2. Instalar el manejador de dependencias de PHP, Composer (versión 1.10.13 en adelante). En el siguiente
link se muestran los pasos a seguir para su correcta instalación:
https://styde.net/instalacion-de-composer/

3. Instalar el manejador de Base de Datos mariadb (10.4.13 en adelante).

En el siguiente link se muestran los pasos a seguir para su correcta instalación en Linux (Ubuntu):
https://www.digitalocean.com/community/tutorials/how-to-install-mariadb-on-ubuntu-18-04-es

En el caso de Windows, se puede instalar usando Wampserver mediante el siguiente link: 
https://www.wampserver.com/en/download-wampserver-64bits/

Cabe destacar que Wampserver trae consigo Apache, PHP y mariadb en sus versiones más recientes.

Posteriormente, crear una base de datos llamada: "foreach".
```

### Configuraciones base para el correcto funcionamiento de la API.
```
1. Ubicarse en la carpeta raíz del proyecto: api-for-each y crear el archivo .env (en caso de no existir). 
Posteriormente reemplazar todo su contenido por el del archivo .env.example

Luego, en el archivo .env hay una serie de llaves que se deben configurar correctamente para realizar una
conexión exitosa con la Base de Datos, entre ellas:

DB_HOST=127.0.0.1
DB_PORT=3307
DB_DATABASE=foreach
DB_USERNAME=root
DB_PASSWORD=1234

Donde DB_HOST es el dominio (dejarlo en 127.0.0.1 o localhost). DB_PORT el puerto a conectar con mariadb.
DB_DATABASE el nombre de la base de datos. Dichos parámetros deben mantenerse tal cual están.

Por último se tienen: DB_USERNAME con el nombre de usuario a conectar, y DB_PASSWORD la contraseña 
asociada a dicho usuario. Cambiar ambos valores de ser necesario a los que correspondan.

2. Se deben instalar todas las dependencias del proyecto haciendo uso de composer con el comando:
"composer install" en la carpeta raíz. Nota importante: se deben considerar los siguientes requerimientos
mínimos durante la instalación de PHP de acuerdo a la documentación oficial ofrecida por Laravel
(https://laravel.com/docs/8.x/installation):

PHP >= 7.3
BCMath PHP Extension
Ctype PHP Extension
Fileinfo PHP Extension
JSON PHP Extension
Mbstring PHP Extension
OpenSSL PHP Extension
PDO PHP Extension
Tokenizer PHP Extension
XML PHP Extension
```

### Ejecución de los comandos necesarios para la puesta en marcha de la API en entorno local.
```
1. php artisan key:generate
2. php artisan jwt:secret
3. php artisan migrate --seed
4. php artisan cache:clear
5. php artisan config:clear
6. composer dump
7. php artisan serve --port 8000 (asegurarse de ejecutar la API en el puerto 8000)
```

### Observaciones generales
```
1. Se cuenta con un servicio de API para inicio de sesión y uno de cierre de la misma.
2. Se cuenta con un servicio de API para listado de medio de transporte.
3. Se cuenta con un servicio de API para listado de viajes.
4. Se cuenta con un servicio de API para registro de viaje.
5. Se realizaron las validaciones base correspondientes en cada servicio de API para mantener la
consistencia adecuada en el flujo de la misma. Del mismo modo, se siguieron los estándares PSR1 y PSR2
para identación del código fuente, junto con buenas prácticas para lectura del código fuente debidamente
documentado.
```
