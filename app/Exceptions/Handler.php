<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Log;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
        \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException::class,
        \Tymon\JWTAuth\Exceptions\TokenExpiredException::class,
        \Tymon\JWTAuth\Exceptions\TokenInvalidException::class,
        \Tymon\JWTAuth\Exceptions\JWTException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $exception)
    {
        if ($request->expectsJson() && method_exists($exception, 'getStatusCode') && method_exists($exception, 'getMessage')) {
            try {
                return response()->json([
                    'error' => [
                        'code' => $exception->getStatusCode(),
                        'http_code' => $exception->getStatusCode(),
                        'message' => $exception->getMessage(),
                    ],
                ], $exception->getStatusCode());
            } catch (Exception $ex) {
                Log::error($ex->getMessage());
            }
        }

        return parent::render($request, $exception);
    }
}
