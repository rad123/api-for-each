<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\LogoutRequest;
use App\Models\User;
use JWTAuth;

/**
 * @group Authentication
 *
 * Allowed actions for authentication resource.
 */
class AuthController extends Controller
{
    /**
     * Login.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest $request
     * @return \Illuminate\Http\Response
     */
    public function login(LoginRequest $request)
    {
        $credentials = [
            'username' => $request->username,
            'password' => $request->password,
        ];

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => __('invalid_credentials')], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => __('could_not_create_token')], 500);
        }

        return response()->json([
            'data' => [
                'user_id' => User::whereUsername($request->username)->firstOrFail()->id,
            ],
            'meta' => [
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => config('jwt.ttl') * 60 * 1000,
                'refresh_expires_in' => config('jwt.refresh_ttl') * 60 * 1000,
            ],
        ]);
    }

    /**
     * Logout.
     *
     * @param  \App\Http\Requests\Auth\LogoutRequest $request
     * @return \Illuminate\Http\Response
     */
    public function logout(LogoutRequest $request)
    {
        JWTAuth::invalidate(JWTAuth::getToken());

        return response()->json('', 204);
    }
}
