<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Conveyance\IndexRequest;
use App\Models\Conveyance;

/**
 * @group Conveyance
 *
 * Allowed actions for conveyance resource.
 */
class ConveyanceController extends Controller
{
    /**
     * Display a list of conveyances.
     *
     * @param  \App\Http\Requests\Conveyance\IndexRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        $conveyances = Conveyance::orderBy('name')->get();

        $data = [];

        foreach ($conveyances as $conveyance) {
            array_push($data, [
                'emission_factor' => $conveyance->emission_factor,
                'id' => $conveyance->id,
                'name' => $conveyance->name,
            ]);
        }

        return response()->json([
            'data' => $data,
        ]);
    }
}
