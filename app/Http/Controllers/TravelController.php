<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Travel\IndexRequest;
use App\Http\Requests\Travel\StoreRequest;
use App\Models\Conveyance;
use App\Models\Travel;

/**
 * @group Travel
 *
 * Allowed actions for travel resource.
 */
class TravelController extends Controller
{
    /**
     * Display a list of travels.
     *
     * @param  \App\Http\Requests\Travel\IndexRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        $travels = Travel::orderBy('number')->get();

        $data = [];

        foreach ($travels as $travel) {
            array_push($data, [
                'address_of_the_starting_point' => $travel->address_of_the_starting_point,
                'conveyance_name' => $travel->conveyance->name,
                'created_at' => !empty($travel->created_at) ? $travel->created_at->toW3cString() : null,
                'end_point_address' => $travel->end_point_address,
                'is_round_trip' => $travel->is_round_trip,
                'kgco2' => number_format($travel->kgco2, 3, ',', '.'),
                'number' => $travel->number,
                'number_of_kilometers' => $travel->number_of_kilometers,
                'number_of_persons_in_travel' => $travel->workers()->count(),
            ]);
        }

        return response()->json([
            'data' => $data,
        ]);
    }

    /**
     * Create a travel instance.
     *
     * @param  \App\Http\Requests\Travel\StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $travel = Travel::create([
            'number' => Travel::nextNumber(),
            'address_of_the_starting_point' => $request->address_of_the_starting_point,
            'end_point_address' => $request->end_point_address,
            'number_of_kilometers' => $request->number_of_kilometers,
            'conveyance_id' => Conveyance::findOrFail($request->conveyance_id)->id,
            'is_round_trip' => $request->is_round_trip,
        ]);

        $travel->setWorkers($request->workers);

        return response()->json([
            'data' => [
                'travel_id' => $travel->id,
            ],
        ], 201);
    }
}
