<?php

namespace App\Http\Requests\Travel;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address_of_the_starting_point' => 'required|string|max:191',
            'end_point_address' => 'required|string|max:191',
            'number_of_kilometers' => 'required|numeric|gt:0',
            'conveyance_id' => 'required|string|exists:conveyances,id',
            'is_round_trip' => 'required|boolean',
            'workers' => 'required|array|min:1',
            'workers.*' => 'required|string|max:191',
        ];
    }
}
