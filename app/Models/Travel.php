<?php

namespace App\Models;

use App\Models\Travel\Accessors;
use App\Models\Travel\Helpers;
use App\Models\Travel\Relationships;

class Travel extends BaseModel
{
    use Accessors, Relationships, Helpers;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_round_trip' => 'boolean',
        'number' => 'integer',
        'number_of_kilometers' => 'double',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'address_of_the_starting_point',
        'conveyance_id',
        'end_point_address',
        'is_round_trip',
        'number',
        'number_of_kilometers',
    ];
}
