<?php

namespace App\Models\Travel;

trait Accessors
{
    /**
     * Define an accessor to get kgco2 attribute.
     *
     * @return double
     */
    public function getKgco2Attribute()
    {
        $numberOfWorkers = $this->workers()->count();
        $roundTrip = $this->is_round_trip ? 2 : 1;
        $conveyanceEmissionFactor = $this->conveyance->emission_factor ?? 0;

        return $numberOfWorkers > 0 ? $this->number_of_kilometers * $conveyanceEmissionFactor * $roundTrip / $numberOfWorkers : 0;
    }
}
