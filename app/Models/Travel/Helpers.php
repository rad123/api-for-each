<?php

namespace App\Models\Travel;

use App\Models\Travel;
use App\Models\Worker;

trait Helpers
{
    /**
     * Get the next number value.
     *
     * @return int
     */
    public static function nextNumber()
    {
        return Travel::max('number') + 1;
    }

    /**
     * Asign workers to current instance and return it modified.
     *
     * @param  array $workers
     * @return \App\Models\Travel
     */
    public function setWorkers($workers)
    {
        $workersIds = [];

        $workers = is_null($workers) ? [] : $workers;

        foreach ($workers as $worker) {
            $worker = Worker::firstOrCreate([
                'name' => $worker,
            ]);

            array_push($workersIds, $worker->id);
        }

        $this->workers()->sync($workersIds);

        return $this;
    }
}
