<?php

namespace App\Models\Travel;

use App\Models\Conveyance;
use App\Models\Worker;

trait Relationships
{
    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function conveyance()
    {
        return $this->belongsTo(Conveyance::class);
    }

    /**
     * Define a many-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function workers()
    {
        return $this->belongsToMany(Worker::class)->withTimestamps();
    }
}
