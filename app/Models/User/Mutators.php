<?php

namespace App\Models\User;

use Hash;

trait Mutators
{
    /**
     * Define a mutator to set password attribute.
     *
     * @param  string $password
     * @return void
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::needsRehash($password) ? Hash::make($password) : $password;
    }
}
