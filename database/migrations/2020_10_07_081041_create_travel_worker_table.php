<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTravelWorkerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel_worker', function (Blueprint $table) {
            $table->uuid('travel_id')->required();
            $table->uuid('worker_id')->required();
            $table->primary(['travel_id', 'worker_id']);
            $table->timestamps();

            $table->foreign('travel_id')
                ->references('id')->on('travel')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table->foreign('worker_id')
                ->references('id')->on('workers')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travel_worker');
    }
}
