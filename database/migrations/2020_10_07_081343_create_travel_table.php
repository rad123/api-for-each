<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTravelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('conveyance_id')->required();
            $table->integer('number')->required();
            $table->string('address_of_the_starting_point')->required();
            $table->string('end_point_address')->required();
            $table->double('number_of_kilometers')->required()->default(0);
            $table->boolean('is_round_trip')->required()->default(false);
            $table->timestamps();

            $table->foreign('conveyance_id')
                ->references('id')->on('conveyances')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travel');
    }
}
