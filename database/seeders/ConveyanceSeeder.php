<?php

namespace Database\Seeders;

use App\Models\Conveyance;
use Illuminate\Database\Seeder;

class ConveyanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $conveyances = [
            [
                'name' => 'Metro (Tren, Subway, Subterráneo)',
                'emission_factor' => 0.033,
            ],
            [
                'name' => 'Auto (Gasolina)',
                'emission_factor' => 0.21,
            ],
            [
                'name' => 'Camioneta (Diésel)',
                'emission_factor' => 0.249,
            ],
            [
                'name' => 'Motocicleta (Gasolina)',
                'emission_factor' => 0.092,
            ],
            [
                'name' => 'Bus Transantiago (Transporte público)',
                'emission_factor' => 0.039,
            ],
            [
                'name' => 'Bus (Vehículo privado)',
                'emission_factor' => 0.012,
            ],
            [
                'name' => 'Avión (Chile)',
                'emission_factor' => 0.279,
            ],
            [
                'name' => 'Avión (Internacional)',
                'emission_factor' => 0.179,
            ],
            [
                'name' => 'Caminando',
                'emission_factor' => 0,
            ],
        ];

        foreach ($conveyances as $conveyance) {
            Conveyance::firstOrCreate($conveyance);
        }
    }
}
