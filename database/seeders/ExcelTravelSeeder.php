<?php

namespace Database\Seeders;

use App\Models\Conveyance;
use App\Models\Travel;
use Illuminate\Database\Seeder;

class ExcelTravelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $travel = Travel::firstOrCreate([
            'number' => 1,
            'address_of_the_starting_point' => 'La niña 3125, Las Condes',
            'end_point_address' => 'Av. Andrés Bello 2425, Providencia',
            'number_of_kilometers' => 10,
            'conveyance_id' => Conveyance::whereName('Metro (Tren, Subway, Subterráneo)')->first()->id,
            'is_round_trip' => true,
        ]);

        $travel->setWorkers([
            'Pedro',
            'Rafael',
        ]);

        $travel = Travel::firstOrCreate([
            'number' => 2,
            'address_of_the_starting_point' => 'Avenida Ricardo Lyon 1060, Providencia',
            'end_point_address' => 'La niña 3125, Las Condes',
            'number_of_kilometers' => 10,
            'conveyance_id' => Conveyance::whereName('Auto (Gasolina)')->first()->id,
            'is_round_trip' => false,
        ]);

        $travel->setWorkers([
            'Jose',
            'Juan',
            'María',
            'Pedro',
            'Rafael',
            'Vanessa',
        ]);

        $travel = Travel::firstOrCreate([
            'number' => 3,
            'address_of_the_starting_point' => 'Samuel Izquierdo 1080, Quinta Normal',
            'end_point_address' => 'La niña 3125, Las Condes',
            'number_of_kilometers' => 10,
            'conveyance_id' => Conveyance::whereName('Bus Transantiago (Transporte público)')->first()->id,
            'is_round_trip' => true,
        ]);

        $travel->setWorkers([
            'Rafael',
        ]);

        $travel = Travel::firstOrCreate([
            'number' => 4,
            'address_of_the_starting_point' => '272-4700 Nulla Ave',
            'end_point_address' => 'P.O. Box 270, 9911 Sem. Rd.',
            'number_of_kilometers' => 600,
            'conveyance_id' => Conveyance::whereName('Avión (Chile)')->first()->id,
            'is_round_trip' => true,
        ]);

        $travel->setWorkers([
            'Jose',
            'Juan',
            'María',
        ]);

        $travel = Travel::firstOrCreate([
            'number' => 5,
            'address_of_the_starting_point' => '859-2099 Euismod St.',
            'end_point_address' => 'P.O. Box 169, 2060 Vel, Rd.',
            'number_of_kilometers' => 43,
            'conveyance_id' => Conveyance::whereName('Metro (Tren, Subway, Subterráneo)')->first()->id,
            'is_round_trip' => false,
        ]);

        $travel->setWorkers([
            'Jose',
            'Juan',
        ]);

        $travel = Travel::firstOrCreate([
            'number' => 6,
            'address_of_the_starting_point' => 'Ap #961-6327 Mauris Street',
            'end_point_address' => 'Ap #574-4576 Metus. Street',
            'number_of_kilometers' => 340,
            'conveyance_id' => Conveyance::whereName('Avión (Chile)')->first()->id,
            'is_round_trip' => true,
        ]);

        $travel->setWorkers([
            'Jose',
            'Juan',
            'María',
        ]);

        $travel = Travel::firstOrCreate([
            'number' => 7,
            'address_of_the_starting_point' => 'P.O. Box 508, 9532 Leo. Av.',
            'end_point_address' => 'Ap #234-5833 Proin Av.',
            'number_of_kilometers' => 623,
            'conveyance_id' => Conveyance::whereName('Avión (Internacional)')->first()->id,
            'is_round_trip' => true,
        ]);

        $travel->setWorkers([
            'Jose',
            'Juan',
        ]);

        $travel = Travel::firstOrCreate([
            'number' => 8,
            'address_of_the_starting_point' => '5509 Et St.',
            'end_point_address' => '466 Arcu Rd.',
            'number_of_kilometers' => 6,
            'conveyance_id' => Conveyance::whereName('Metro (Tren, Subway, Subterráneo)')->first()->id,
            'is_round_trip' => false,
        ]);

        $travel->setWorkers([
            'Jose',
        ]);

        $travel = Travel::firstOrCreate([
            'number' => 9,
            'address_of_the_starting_point' => '8592 Adipiscing St.',
            'end_point_address' => '260-3301 At, St.',
            'number_of_kilometers' => 4,
            'conveyance_id' => Conveyance::whereName('Bus (Vehículo privado)')->first()->id,
            'is_round_trip' => false,
        ]);

        $travel->setWorkers([
            'Alberto',
            'Alfonso',
            'Angel',
            'David',
            'Elena',
            'Enrique',
            'Fuente',
            'Gustavo',
            'Isabel',
            'Jose',
            'Juan',
            'Luis',
            'María',
            'Miguel',
            'Miko',
            'Pedro',
            'Rafael',
            'Rony',
            'Rosa',
            'Vanessa',
        ]);

        $travel = Travel::firstOrCreate([
            'number' => 10,
            'address_of_the_starting_point' => 'P.O. Box 448, 3825 Convallis, Street',
            'end_point_address' => 'P.O. Box 448, 3825 Convallis, Street',
            'number_of_kilometers' => 21,
            'conveyance_id' => Conveyance::whereName('Auto (Gasolina)')->first()->id,
            'is_round_trip' => true,
        ]);

        $travel->setWorkers([
            'Alberto',
            'Alfonso',
            'Angel',
        ]);
    }
}
