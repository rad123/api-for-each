<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class FixSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserAdminSeeder::class);
        $this->call(ConveyanceSeeder::class);
        $this->call(ExcelTravelSeeder::class);
    }
}
