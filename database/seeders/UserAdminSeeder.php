<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userAttributes = [
            'username' => 'admin',
        ];

        User::withTrashed()->updateOrCreate($userAttributes, array_merge($userAttributes, [
            'password' => 'secret',
        ]));
    }
}
