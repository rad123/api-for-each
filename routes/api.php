<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

/*
 * Routes defined to authenticate.
 */
Route::group(['prefix' => 'auth', 'as' => 'auth'], function () {
    Route::post('/login', ['as' => '.login', 'uses' => 'AuthController@login']);
    Route::delete('/logout', ['as' => '.logout', 'uses' => 'AuthController@logout'])->middleware('jwt.auth');
});

/*
 * Routes defined to conveyances.
 */
Route::group(['prefix' => 'conveyances', 'as' => 'conveyances', 'middleware' => ['jwt.auth']], function () {
    Route::get('/', ['as' => '.index', 'uses' => 'ConveyanceController@index']);
});

/*
 * Routes defined to travels.
 */
Route::group(['prefix' => 'travels', 'as' => 'travels', 'middleware' => ['jwt.auth']], function () {
    Route::get('/', ['as' => '.index', 'uses' => 'TravelController@index']);
    Route::post('/store', ['as' => '.store', 'uses' => 'TravelController@store']);
});
